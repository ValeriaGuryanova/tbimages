
import torch
from torch.nn import Softmax
from sklearn.metrics import roc_auc_score
from early_stopping import EarlyStopping
from torch.utils.tensorboard import SummaryWriter
import pandas as pd
import numpy as np
from collections import defaultdict
import os
from sklearn.metrics import auc
from sklearn.metrics import precision_recall_curve
import datetime

gl_epoch = 0
max_j = 0


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']

class Trainer:

    def __init__(self, output_file, device, model, loss, train_loader, val_loader, test_loader, 
                 log_dir, isBinary=False,
                 binary_threshold=0.5, stop_metric="RocAuc", run_dir="runs"):

        self.device = device
        self.model = model
        global gl_epoch
        gl_epoch = 0
        self.train_loader = train_loader
        self.val_loader = val_loader
        self.test_loader = test_loader
        self.output_file = output_file
        self.loss = loss
        self.isBinary = isBinary
        self.binary_threshold = binary_threshold
        self.log_dir = log_dir
        self.stop_metric = stop_metric
        current_time = datetime.datetime.now()
        #self.val_metrics = SummaryWriter(log_dir = run_dir + "/" + str(current_time), comment='_' + self.log_dir + "_val")
        self.metrics = SummaryWriter(log_dir = run_dir + "/" + str(current_time) + self.log_dir)
        #self.test_metrics = SummaryWriter(log_dir = run_dir + "/" + str(current_time), comment='_' + self.log_dir + "_test")

    def train_model(self, optimizer, scheduler, epochs, patience=10, loss=None):
        early_stopping = EarlyStopping(output_file=self.output_file, patience=patience, verbose=True)
        global_step = 0
      
        for i in range(epochs):
            global gl_epoch
            print(get_lr(optimizer))
            print("Epoch: {}/{}".format(i, epochs))
            self.model.train()
            overall_loss = 0
            overall_accuracy = 0
            total_number = 0
            softmax = Softmax(dim=1)
            self.model.train()
            prediction_values = []
            pred_labels = []

            for j, (path, inputs, labels) in enumerate(self.train_loader):
                global_step += 1
                inputs = inputs.to(self.device)
                labels = labels.to(self.device)
 
                outputs = self.model(inputs)
                predictions = softmax(outputs)
                loss_value = self.loss(outputs, labels)
                _, preds = torch.max(predictions, 1)

                prediction_values.append(predictions[:, 1].detach().cpu())
                pred_labels.append(labels.cpu())


                accuracy = torch.sum(preds.detach() == labels)

                overall_loss += loss_value.detach().cpu()
                overall_accuracy += accuracy
                total_number += inputs.shape[0]

                optimizer.zero_grad()
                loss_value.backward()
                optimizer.step()
                try:
                    roc_auc_step = roc_auc_score(labels.cpu(), predictions[:, 1].detach().cpu())
                    precision, recall, thresholds = precision_recall_curve(labels.cpu(), predictions[:, 1].detach().cpu())
                    pr_auc_step = auc(recall, precision)
                    #print(pr_auc_step)
                    #print(roc_auc_step)
                    #print('Roc AUC', roc_auc_step, global_step)
                    self.metrics.add_scalar('Roc AUC/train', roc_auc_step, global_step)
                    self.metrics.add_scalar('PR AUC/train', pr_auc_step, global_step)
                except Exception:
                    #print("here", j)
                    pass

                self.metrics.add_scalar('Loss/train', loss_value, global_step)
                self.metrics.add_scalar('Accuracy/train', accuracy.double() / inputs.shape[0], global_step)
                #print('Accuracy', accuracy.double() / inputs.shape[0], global_step)
            if scheduler:
                scheduler.step(i)
            
           
            

            epoch_acc = float(overall_accuracy) / total_number
            epoch_loss = overall_loss / j
            epoch_roc_auc = roc_auc_score(torch.cat(pred_labels), torch.cat(prediction_values))
            precision, recall, thresholds = precision_recall_curve(torch.cat(pred_labels), torch.cat(prediction_values))
            epoch_pr_auc = auc(recall, precision)
            
            self.metrics.add_scalar('Epoch Acc/train', epoch_acc, gl_epoch)
            self.metrics.add_scalar('Epoch Loss/train', epoch_loss, gl_epoch)
            self.metrics.add_scalar('Epoch Roc AUC/train', epoch_roc_auc, gl_epoch)
            self.metrics.add_scalar('Epoch PR AUC/train', epoch_pr_auc, gl_epoch)

            print("Train loss: {}, accuracy:  {}, roc-auc: {}, pr-auc: {} ".format(epoch_loss, epoch_acc, epoch_roc_auc, epoch_pr_auc))
            epoch_loss, epoch_acc, epoch_roc_auc, epoch_pr_auc, _, _, _, _ = self.evaluate("val")
            print("Val loss: {}, accuracy: {}, roc-auc: {}, pr-auc: {}".format(epoch_loss, epoch_acc, epoch_roc_auc, epoch_pr_auc))
            test_epoch_loss, test_epoch_acc, test_epoch_roc_auc, test_epoch_pr_auc, _, _, _, _ = self.evaluate("test")
            print("Test loss: {}, accuracy: {}, roc-auc: {}, pr-auc: {}".format(test_epoch_loss, test_epoch_acc, test_epoch_roc_auc, test_epoch_pr_auc))
            
            
                
            #if (scheduler):
            #    scheduler.step(epoch_loss)
            gl_epoch += 1
            if self.stop_metric == "RocAuc":
                early_stopping(epoch_roc_auc, self.model)
            elif self.stop_metric == "PrAuc":
                early_stopping(epoch_roc_auc, self.model)
            else:
                print("Bad Metric No Stopping")
            if early_stopping.early_stop:
                print("Early stopping")
                break
                
        chekpoint =torch.load(self.output_file)
        print("load", self.output_file)
        self.model.load_state_dict(chekpoint)
        return self.model

    


    def evaluate(self, mode):
        print("--"+mode+"--")
        if mode == "test":
            loader = self.test_loader
        else:
            loader = self.val_loader
        
        with torch.no_grad():
            self.model.eval()
            test_labels = []
            pred_labels = []
            overall_val_loss = 0
            overall_val_accuracy = 0
            total_val_number = 0
            softmax = Softmax(dim=1)
            prediction_values = []
            paths = []
            global max_j
            for j, (path, inputs, labels) in enumerate(loader):
                inputs = inputs.to(self.device)
                labels = labels.to(self.device)
                outputs = self.model(inputs)
                loss_value = self.loss(outputs, labels)
                predictions = softmax(outputs)
                _, preds = torch.max(predictions, 1)
               
                accuracy = torch.sum(preds == labels)
                    
                prediction_values.append(predictions[:, 1].detach().cpu())
                test_labels.append(labels.cpu())
                pred_labels.append(preds.cpu())
                paths = paths + list(path)
                overall_val_loss += loss_value
                overall_val_accuracy += accuracy
                total_val_number += inputs.shape[0]
                if mode == "val":
                    self.metrics.add_scalar('Loss/val', loss_value, max_j * gl_epoch + j)
                    self.metrics.add_scalar('Accuracy/val', accuracy.double() / inputs.shape[0], max_j * gl_epoch + j)
                else:
                    self.metrics.add_scalar('Loss/test', loss_value, max_j * gl_epoch + j)
                    self.metrics.add_scalar('Accuracy/test', accuracy.double() / inputs.shape[0], max_j * gl_epoch + j)
                try:
                    roc_auc_step = roc_auc_score(labels.cpu(), predictions[:, 1].detach().cpu())
                    precision, recall, thresholds = precision_recall_curve(labels.cpu(), predictions[:, 1].detach().cpu())
                    pr_auc_step = auc(recall, precision)
                    if mode == "val":
                        self.metrics.add_scalar('Roc AUC/val', roc_auc_step, max_j * gl_epoch + j)
                        self.metrics.add_scalar('PR AUC/val', pr_auc_step, max_j * gl_epoch + j)
                    else:
                        self.metrics.add_scalar('Roc AUC/test', roc_auc_step, max_j * gl_epoch + j)
                        self.metrics.add_scalar('PR AUC/test', pr_auc_step, max_j * gl_epoch + j)
                except Exception:
                    pass
                    
            
            max_j = j
           
            epoch_acc = float(overall_val_accuracy) / total_val_number
            epoch_loss = overall_val_loss / j
            epoch_roc_auc = roc_auc_score(torch.cat(test_labels), torch.cat(prediction_values)) 
            precision, recall, thresholds = precision_recall_curve(torch.cat(test_labels), torch.cat(prediction_values))
            epoch_pr_auc = auc(recall, precision)
            if mode == "val":
                self.metrics.add_scalar('Epoch Acc/val', epoch_acc, gl_epoch)
                self.metrics.add_scalar('Epoch Loss/val', epoch_loss, gl_epoch)
                self.metrics.add_scalar('Epoch Roc AUC/val', epoch_roc_auc, gl_epoch)
                self.metrics.add_scalar('Epoch PR AUC/val', epoch_pr_auc, gl_epoch)
            elif mode == "test":
                self.metrics.add_scalar('Epoch Acc/test', epoch_acc, gl_epoch)
                self.metrics.add_scalar('Epoch Loss/test', epoch_loss, gl_epoch)
                self.metrics.add_scalar('Epoch Roc AUC/test', epoch_roc_auc, gl_epoch)
                self.metrics.add_scalar('Epoch PR AUC/test', epoch_pr_auc, gl_epoch)
            #print("pr", prediction_values[:5])
            #print("test", test_labels[:5])
            return epoch_loss, epoch_acc, epoch_roc_auc, epoch_pr_auc,\
                   torch.cat(test_labels), torch.cat(pred_labels), torch.cat(prediction_values), paths

    def save_test_results(self, test_labels, pred_labels, predictions, paths, output_path):
        print(output_path)
        result = pd.DataFrame(
            {"labels": test_labels.detach().cpu().numpy(), "pred_labels": pred_labels.detach().cpu().numpy(),
             "predictions": predictions.detach().cpu().numpy(), "paths": paths})
        result.to_excel(output_path, index=False)

    
    def save(self, filename):
        torch.save(self.model.state_dict(), filename)

    def load(self, filename):
        self.model.load_state_dict(torch.load(filename))
        
