from torchvision import models
from torch import nn
import torch


class SimplePretrainedModel:
    def __init__(self, device, model_name, n_classes, pretrained, freeze_layer):
        self.model_name = model_name
        self.n_classes = n_classes
        self.device = device
        self.pretrained = pretrained
        self.freeze_layer = freeze_layer

    def generate_model(self, isTrain=True):
        if (self.model_name == 'resnext101_32x8d'):
            model = ResNext(self.n_classes, self.pretrained, self.freeze_layer)
            model = model.to(self.device)
            model = model.type(torch.cuda.FloatTensor)
            return model


        elif (self.model_name == 'densenet121'):

            model = models.densenet121(pretrained=True)

            for ind, param in enumerate(model.named_parameters()):
                if ('denseblock4' in param[0]):
                    param[1].requires_grad = True
                else:
                    param[1].requires_grad = False

            num_ftrs = 1024
            model.classifier = nn.Linear(model.classifier.in_features, self.n_classes)
            model = model.to(self.device)
            model = model.type(torch.cuda.FloatTensor)
            return model
        else:
            raise Exception("Such type of model not supported")






class ResNext(nn.Module):

    def __init__(self, n_classes, pretrained, freeze_layer):

        super(ResNext, self).__init__()
        # define the resnet152
        self.resnet = models.resnext101_32x8d(pretrained=pretrained)
        
        for ind, param in enumerate(self.resnet.named_parameters()):
            layer = param[0].split('.')[0]
            # param[1].requires_grad = True
            if freeze_layer == 0:
                param[1].requires_grad = True
            else:
                if layer in ["layer" + str(i) for i in range(freeze_layer+1, 5)]:
                    #print("unfreezed", layer)
                    param[1].requires_grad = True
                else:
                    #print("freezed", layer)
                    param[1].requires_grad = False

        # isolate the feature blocks
        self.features = nn.Sequential(self.resnet.conv1,
                                      self.resnet.bn1,
                                      nn.ReLU(),
                                      nn.MaxPool2d(kernel_size=3, stride=2, padding=1, dilation=1, ceil_mode=False),
                                      self.resnet.layer1,
                                      self.resnet.layer2,
                                      self.resnet.layer3,
                                      self.resnet.layer4)

        self.avgpool = self.resnet.avgpool
        self.num_ftrs = self.resnet.fc.in_features
        self.classifier = nn.Linear(self.num_ftrs, n_classes)
        # gradient placeholder
        self.gradient = None

    # hook for the gradients

    def activations_hook(self, grad):
        self.gradient = grad

    def get_gradient(self):
        return self.gradient

    def get_activations(self, x):
        return self.features(x)

    def forward(self, x):
        x = self.features(x)
        # register the hook
        if x.requires_grad:
            h = x.register_hook(self.activations_hook)
        # complete the forward pass
        x = self.avgpool(x)
        x = x.view(x.size(0), -1)
        x = self.classifier(x)
        return x
