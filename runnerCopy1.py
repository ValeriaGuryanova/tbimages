import json
import os
from trainerCopy1 import Trainer
from tb_datasets import TBImageDataSet
from torchvision import transforms
from torch.utils.data import DataLoader
import models
from models import SimplePretrainedModel
import torch
from torch import optim
import torch.nn as nn
from metrics import save_metrics
from tranformations_mixup import CrossEntropyLoss
import numpy as np
import random


class Runner:
    def __init__(self, config_name):
    
        with open(config_name, "r") as read_file:
            
            self.config = json.load(read_file)
            self.experiment_name = self.config["experiment_name"]
            self.early_stopping = self.config["early_stopping"]
            self.data_dir = self.config["data_dir"]
            image_size_batch_size = self.config["image_size+batch_size"]
            self.batch_size = int(image_size_batch_size[1])
            self.stop_metric = self.config["stop_metric"]
            self.run_dir = self.config["run_dir"]
            
            self.image_size = int(image_size_batch_size[0])
            self.test = self.config["test"]
            self.result_path = self.config["result_path"]
            self.pretrained = self.config["pretrained"]
            self.freeze_layer = self.config["freeze_layer"]
            self.milestones = [int(i) for i in self.config["milestones"]]
            print(self.milestones)
            self.gamma = self.config["gamma"]
            
            print(self.batch_size)
            print(self.image_size)

            self.balance_technique = self.config["balance_technique"]
            self.oversampling_percent = self.config["oversampling_percent"]
            self.result_path = self.config["result_path"]

            self.model_name = self.config["model_name"]
            self.n_classes = self.config["n_classes"]

            self.optimizer = self.config['optimizer']
            self.random_seed(self.config['random_seed'])
            self.make_result_dirs()
            

    def random_seed(self, seed_value, use_cuda=True):
        np.random.seed(seed_value)  # cpu vars
        torch.manual_seed(seed_value)  # cpu vars
        random.seed(seed_value)  # Python
        if use_cuda:
            torch.cuda.manual_seed(seed_value)
            torch.cuda.manual_seed_all(seed_value)  # gpu vars
            torch.backends.cudnn.deterministic = True  # needed
            torch.backends.cudnn.benchmark = False

    def make_result_dirs(self):
        if (not os.path.exists(self.result_path)):
            os.mkdir(self.result_path)
            os.mkdir(os.path.join(self.result_path, "models"))
            os.mkdir(os.path.join(self.result_path, "analytics"))
            os.mkdir(os.path.join(self.result_path, "errors"))
            os.mkdir(os.path.join(self.result_path, "results"))
            os.mkdir(os.path.join(self.result_path, "data"))

    def balancing(self):
        balancing_method = getattr(dataset_utills, self.balance_technique)
        new_data_dir = os.path.join(self.result_path, "data")
        balancing_method(self.data_dir, new_data_dir, self.oversampling_percent)
        self.data_dir = new_data_dir

    def run(self):
      
        TRAIN_DATA = os.path.join(self.data_dir, '1new_image_train_f.csv')
        VAL_DATA = os.path.join(self.data_dir, '1new_image_val_f.csv')
        TEST_DATA = os.path.join(self.data_dir, '1new_image_test_f.csv')
        report = self.train(TRAIN_DATA, VAL_DATA, TEST_DATA)
        return report    

    def train(self, train_data_path, val_data_path, test_data_path):
        self.make_result_dirs()
#        device = torch.device("cuda:" + self.config["cuda"] if torch.cuda.is_available() else "cpu")
        device = torch.device('cuda')

        '''
        if self.transofrms:
            transform_method = getattr(custom_transforms, self.transforms)
            transformations = transform_method(self.image_size)
        else:
            transformations = None
        '''
        transformations = None
     
        train_dataset = TBImageDataSet(train_data_path,self.image_size, transformations)
        val_dataset = TBImageDataSet(val_data_path, self.image_size, transformations)

        train_loader = DataLoader(train_dataset, batch_size=self.batch_size, num_workers=40, shuffle=True)
        val_loader = DataLoader(val_dataset, batch_size=self.batch_size, num_workers=40, shuffle=True)
        
        test_dataset = TBImageDataSet(test_data_path, self.image_size, transformations)
        test_loader = DataLoader(test_dataset, batch_size=self.batch_size, num_workers=40, shuffle=True)

        model = SimplePretrainedModel(device, self.model_name, 2, self.pretrained, self.freeze_layer)
        model = model.generate_model()
        model = torch.nn.DataParallel(model)
        criterion = nn.CrossEntropyLoss()

        optimizer = getattr(optim, self.optimizer)(model.parameters(), lr=self.config["learning_rate"],
                                                   weight_decay=self.config["weight_decay"])
        #scheduler = optim.lr_scheduler.MultiStepLR(optimizer=optimizer,  milestones = [2, 4], gamma=0.001)
        scheduler = optim.lr_scheduler.MultiStepLR(optimizer=optimizer,  milestones = self.milestones, gamma=self.gamma)

        output_model_path = os.path.join(self.result_path, "models",  "_" + self.model_name + '.pth')

        trainer = Trainer(output_model_path, device, model, criterion, train_loader, val_loader, test_loader,
                          self.experiment_name + "_", stop_metric=self.stop_metric, run_dir=self.run_dir)
        
        model = trainer.train_model(optimizer, scheduler, epochs=self.config["epochs"], patience=self.config["patience"])

        epoch_loss, epoch_acc,  epoch_roc_auc, test_pr_auc, test_labels, test_pred_labels, test_predictions, test_paths = trainer.evaluate("test")
        epoch_loss, epoch_acc,  epoch_roc_auc, val_pr_auc, val_labels, val_pred_labels, val_predictions, val_paths = trainer.evaluate("val")
        
        trainer.save_test_results(test_labels, test_pred_labels, test_predictions, test_paths,
                                  os.path.join(self.result_path, "results", "test_tb_detection" + ".xls"))
        trainer.save_test_results(val_labels, val_pred_labels, val_predictions, val_paths,
                                  os.path.join(self.result_path, "results", "val_tb_detection" + ".xls"))
        #trainer.save(os.path.join(self.result_path, "models",  "_" + self.model_name + '__last.pth'))
        val_roc_auc, val_report = save_metrics(val_labels, val_pred_labels, 
                                               val_predictions, os.path.join(self.result_path, "analytics", "val_log.txt"),
                     self.model_name)
        test_roc_auc, test_report = save_metrics(test_labels, test_pred_labels, 
                                                 test_predictions, os.path.join(self.result_path, "analytics", "test_log.txt"),
                     self.model_name)
        
        return val_roc_auc, val_pr_auc, val_report, test_roc_auc, test_pr_auc, test_report
