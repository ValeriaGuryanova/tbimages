from sklearn.metrics import classification_report
from sklearn.metrics import roc_auc_score, roc_curve
import numpy as np
from sklearn.metrics import f1_score
import torch
from torch.nn import Softmax
import pandas as pd
from sklearn.metrics import fbeta_score
from tqdm import tqdm




def save_metrics(test_labels, pred_labels, predictions, file_name, model_name):
    print("---save metrics---")
    file = open(file_name, "a")
    roc_auc = roc_auc_score(test_labels.cpu(), predictions.cpu().numpy())
    report = classification_report(test_labels.cpu(), pred_labels.cpu())
    file.write('Model name : ' + model_name + '\n')
    file.write('ROC AUC: ' + str(roc_auc) + '\n')
    file.write('Classification report: ' + '\n')
    file.write(report + '\n')
    file.close()
    print('Model name : ' + model_name + '\n')
    print('ROC AUC: ' + str(roc_auc) + '\n')
    print('Classification report: ' + '\n')
    print(report + '\n')
    return roc_auc, report


def find_optimal_cutoff_youden(target, predicted):
    """ Find the optimal probability cutoff point for a classification model related to event rate
    Parameters
    ----------
    target : Matrix with dependent or target data, where rows are observations
    predicted : Matrix with predicted data, where rows are observations
    Returns
    -------
    list type, with optimal cutoff value
    """
    fpr, tpr, threshold = roc_curve(target, predicted)
    i = np.arange(len(tpr))
    roc = pd.DataFrame({'tf': pd.Series(tpr - (1 - fpr), index=i), 'threshold': pd.Series(threshold, index=i)})
    roc_t = roc.ix[(roc.tf - 0).abs().argsort()[:1]]
    print(roc_t)
    return roc_t['threshold'].values[0]


def find_optimal_cutoff_bestf1(target, predicted):
    threshold_map = {}
    for threshold in np.arange(0.0, 1.0, 0.0001):
        test_preds = np.where(predicted > threshold, 1, 0)
        f1 = f1_score(target, test_preds)
        threshold_map[threshold] = f1
    threshold_map = sorted(threshold_map.items(), key=lambda kv: kv[1], reverse=True)
    threshold = next(iter(threshold_map))[0]
    return threshold


def find_optimal_cutoff_fbeta_score(target, predicted,beta):
    f_scores = []
    a = np.arange(0.0, 1.0, 0.00005)
    for i in tqdm(a):
        test_preds = np.where(predicted > i, 1, 0)
        f_beta = fbeta_score(target, test_preds,beta)
        f_scores.append(f_beta)
    return a[np.argmax(f_scores)]

def find_optimal_threshold(test_labels, predictions, file_name, model_name, data_name, youden=True,beta=None):
    file = open(file_name, "a")
    #predictions = predictions[:, 1].detach().cpu().numpy()
    roc_auc = roc_auc_score(test_labels, predictions)
    print("ROC AUC: ", roc_auc)
    if (youden):
        threshold = find_optimal_cutoff_youden(test_labels, predictions)
    else:
        threshold=find_optimal_cutoff_fbeta_score(test_labels, predictions,beta)
    print(threshold)
    test_preds = np.where(predictions > threshold, 1, 0)
    report = classification_report(test_labels, test_preds)
    file.write('Model name : ' + model_name + '\n')
    file.write('Data : ' + data_name + '\n')
    file.write('ROC AUC: ' + str(roc_auc) + '\n')
    file.write('Threshold: ' + str(threshold) + '\n')
    file.write('Classification report: ' + '\n')
    file.write(report + '\n')
    file.close()
    print('Model name : ' + model_name + '\n')
    print('Data : ' + data_name + '\n')
    print('ROC AUC: ' + str(roc_auc) + '\n')
    print('Threshold: ' + str(threshold) + '\n')
    print('Classification report: ' + '\n')
    print(report + '\n')
    return threshold



def calculate_metrics_with_treshold(test_labels, predictions, file_name, model_name, data_name,threshold):
    file = open(file_name, "a")
    #predictions = predictions[:, 1].detach().cpu().numpy()
    roc_auc = roc_auc_score(test_labels, predictions)
    print("ROC AUC: ", roc_auc)

    test_preds = np.where(predictions > threshold, 1, 0)

    report = classification_report(test_labels, test_preds)
    file.write('Model name : ' + model_name + '\n')
    file.write('Data : ' + data_name + '\n')
    file.write('ROC AUC: ' + str(roc_auc) + '\n')
    file.write('Threshold: ' + str(threshold) + '\n')
    file.write('Classification report: ' + '\n')
    file.write(report + '\n')
    file.close()
    print('Model name : ' + model_name + '\n')
    print('Data : ' + data_name + '\n')
    print('ROC AUC: ' + str(roc_auc) + '\n')
    print('Threshold: ' + str(threshold) + '\n')
    print('Classification report: ' + '\n')
    print(report + '\n')
    return threshold


def sort_data(data, column_name, is_reversed):
    col = data.ix[:, column_name]
    temp = np.array(col.values.tolist())
    order = sorted(range(len(temp)), key=lambda j: float('inf') if temp[j] is None else np.abs(temp[j]),
                   reverse=is_reversed)
    data = data.ix[order]
    return data




