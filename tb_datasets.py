from PIL import Image, ImageFile
import numpy as np
from torch.utils.data import Dataset
import pandas as pd
from torchvision import datasets, models, transforms
import datetime
import torchvision





class TBImageDataSet(Dataset):
    """TB DataSet"""

    def __init__(self, file, image_size, transform=None):
        self.data = pd.read_csv(file)
        #self.data = self.data.sample(500, random_state=20)
        self.data.reset_index(inplace=True, drop=True)
        self.transform = transform
        self.image_size = image_size

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        
        img_path = self.data.loc[idx, 'path']
        img = None
        label = int(self.data.loc[idx, 'label'])
        mean = np.load("mean_img.npy")
        std = np.load("std_img.npy")
        try:
            img = np.load(img_path +".npy")
            #img = (img -img.min() )/ (img.max() - img.min())
            #print(img.shape)
            img = img[:, 61:1128]
            img = Image.fromarray(np.repeat((255 - img[:,:,3])[:,:,np.newaxis], repeats=3, axis = 2), "RGB")
            simple_transform =  transforms.Compose([
                                transforms.Resize((self.image_size, self.image_size)),
                                transforms.ToTensor()])
                                #torchvision.transforms.Normalize(
                                #      [0.485, 0.456, 0.406], [0.229, 0.224, 0.225])])
            

            img = simple_transform(img)
            #totensor = transforms.ToTensor()
            #img = totensor(img)
            if self.transform:

                if (isinstance(self.transform, albumentations.core.composition.Compose)):
                    array = np.asarray(img)
                    img = self.transform(image=array)
                    img = img['image']
                    totensor = transforms.ToTensor()
                    img = totensor(img)

                else:
                    img = self.transform(img)
        #    break
        # print(img)
        # array   = np.asarray(img)
        except Exception:
            print(img_path)
  
        return img_path, img, label
